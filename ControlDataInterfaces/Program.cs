﻿using System;
using System.IO;
using System.Text;

namespace ControlDataInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = DateTime.Now;
            if (args.Length == 1) date = Convert.ToDateTime(args[0]);
            Console.WriteLine("Date work: {0}", date.ToShortDateString());

            ControlFiles file = new ControlFiles();

            string datePattern = date.Year.ToString("0000") + date.Month.ToString("00") + date.Day.ToString("00");
            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory(), datePattern+"_*.txt");
            Console.WriteLine("Files count: {0}",files.Length);
            Array.Sort(files, StringComparer.InvariantCulture);

            int foundLastUnderscore = 0;
            int foundLastPoint = 0;
            int interfaceNameLength = 0;
            int linesCount = 0;
            string nameInterfaz = string.Empty;

            foreach (string _file in files)
            {
                if (_file.Contains("_BCP")) continue;
                if (_file.Contains("PGNOTASCARGODIA")) continue;

                foundLastUnderscore = _file.LastIndexOf("_") + 1;
                foundLastPoint = _file.LastIndexOf(".");
                interfaceNameLength = foundLastPoint - foundLastUnderscore;
                Console.WriteLine("ineterface name length: {0}", _file.Length);
                nameInterfaz = _file.Substring(foundLastUnderscore, interfaceNameLength).ToUpper();
                Console.WriteLine("Name Interface: {0}", nameInterfaz);

                linesCount = File.ReadAllLines(_file).Length;
                if (linesCount == 0) continue;
                nameInterfaz = GetInterfaceName(nameInterfaz);

                var foundDatePattern = _file.IndexOf(datePattern);
                var timeFile = _file.Substring(foundDatePattern, 14).ToUpper();

                file.WriteText(nameInterfaz+","+linesCount+","+timeFile+"\n");                
            }            
        }

        public static string GetInterfaceName(string filename)
        {
            string interfacename = string.Empty;

            switch (filename)
            {
                case "PGPRVCAT":
                    interfacename = "Proveedores";
                    break;
                case "PGMOVTOS":
                    interfacename = "Movimientos";
                    break;
                case "PGCHEQUE":
                    interfacename = "Pagos cabecera";
                    break;
                case "PGMOVTOS4":
                    interfacename = "Pagos detalles";
                    break;
                case "COMPENSADOS":
                    interfacename = "Compensados";
                    break;
                case "CANC":
                    interfacename = "Pagos cancelados";
                    break;
                default:
                    interfacename = filename;
                    break;
            }

            return interfacename;
        }
    }

    public class ControlFiles
    {
        public string DirectoryWork { get; set; }
        public DateTime DateCreation { get; set; }

        private string filePath = string.Empty;
        public ControlFiles()
        {
            DirectoryWork = Directory.GetCurrentDirectory();
            DateCreation = DateTime.Now;

            BuildFileNames();
        }
        public void BuildFileNames()
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            string fileName = DateCreation.Year.ToString("0000") + DateCreation.Month.ToString("00") + DateCreation.Day.ToString("00") + "_" + DateCreation.Hour.ToString("00") + DateCreation.Minute.ToString("00") + DateCreation.Second.ToString("00") + "_controldatainterfaces.csv";
            filePath = fileName;
            string fileheaders = "Interface,Cantidad,Hora ejecucion\n";
            WriteText(fileheaders);
            Console.WriteLine("wrote file hedaers");
        }

        public void WriteText(string text)
        {
            try
            {
                byte[] encodedText = Encoding.Unicode.GetBytes(text);
                StreamWriter fwriter = new StreamWriter(filePath, true)
                {
                    AutoFlush = true
                };
                fwriter.Write(text);
                fwriter.Flush();
                fwriter.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
